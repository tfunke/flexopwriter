This is a slightly modified copy of the pandas ExcelWriter engine
'openpyxl' used by to_excel. It changes the handling of already
existing .xlsx files. It will not delete already existing worksheets.

Adds the option 'force' that, if set to False, only writes the
values to the Excel sheet without applying/changing the cell style.

The name of the engine is 'flex'.

    Usage:
    ------
    Copy flexopWriter.py and make it available for import.

    >>> from pandas import ExcelWriter
    >>> from flexopWriter import _flexopWriter

    Your linter might warn that it´s imported but never used. Just ignore this
    message.

    Then you can use it like this.

    >>> with ExcelWriter('path_to_file.xlsx',
                engine = 'flex',
                mode = {'w'|'a'},
                force = {True|False},
                ) as writer:
    ...     df.to_excel(writer, sheet_name='SheetName')

    If 'path_to_file.xlsx' already exists, all existing sheets with names
    different from 'SheetName' will not be touched and there are the
    following options:

    If 'SheetName' already exists:

        mode = 'w' (default):
            -> 'SheetName' will be overwritten
        mode = 'a':
            -> 'SheetName1' will be appended
            (unless 'SheetName1' already exists. Then another 1 will be
            appended ...)

    If 'SheetName' does not exist, it will be appended independent of mode.

    Force = True (default):
        values and cell style are written
    Force = False
        only values are written

    If 'path_to_file.xlsx' does not exist, it will be created with the sheet
    'SheetName'.

test_flex.py and test_overwrite.py implement some pytest based tests.

As I am new in this game and a bit scared to do something wrong with licenses
and copyrights, I want to be very clear.
The flexopWriter, its documentation and the corresponding tests are based on copies
of the original pandas openpyxl engine (pandas version 0.25). Just made some
changes to make it work according to my wishes. If you use, change and distribute
it, please respect the copyrights and licenses from the pandas Development Team
and others that might apply.
Please refer to the license section.