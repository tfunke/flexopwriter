import pytest

# from flexopWriter import _flexopWriter

from pandas import DataFrame
import pandas.util.testing as tm
from datetime import date, timedelta, datetime
from pandas.io.excel import ExcelWriter
from openpyxl import Workbook, styles
from openpyxl.styles import colors

openpyxl = pytest.importorskip("openpyxl")

pytestmark = pytest.mark.parametrize("ext", [".xlsx"])

header = ["Index", "Dates", "Integers", "Strings"]
dates = list(date.today() + timedelta(days=n) for n in range(4))
ints = list(range(1, 5))
strings = ["string1", "string2", "string3", "string4"]
df_base = DataFrame(
    zip(dates, ints, strings),
    index=["I0", "I1", "I2", "I3"],
    columns=header[1:],
)


@pytest.mark.parametrize("force", (True, False))
def test_existing_file_with_style(ext, force):
    wb = Workbook()
    ws1 = wb.active
    fill = styles.PatternFill(fill_type="solid", fgColor=colors.YELLOW)
    fill_default = styles.PatternFill(fill_type=None)
    for row in range(1, 6):
        for col in range(1, 5):
            ws1.cell(
                row=row,
                column=col,
                value=(header[col - 1] if row == 1 else ""),
            ).fill = fill
    ws1.title = "test1"
    with tm.ensure_clean(ext) as f:
        wb.save(filename=f)

        writer = ExcelWriter(f, engine="flex", force=force)
        df_base.to_excel(writer, sheet_name="test1", index=True)
        writer.save()

        wb2 = openpyxl.load_workbook(f)
        assert wb.worksheets[0].title == "test1"
        ws = wb2["test1"]
        assert ws.cell(row=1, column=1).value == (
            header[0] if not force else None
        )
        for col in range(2, df_base.shape[1] + 2):
            assert ws.cell(row=1, column=col).value == header[col - 1]
            assert ws.cell(row=1, column=col).fill == (
                fill if not force else fill_default
            )
        for row in range(2, df_base.shape[0] + 2):
            for col in range(1, df_base.shape[1] + 2):
                val_in = ws.cell(row=row, column=col).value
                val_comp = (
                    df_base.iloc[row - 2, col - 2]
                    if col > 1
                    else list(df_base.index)[row - 2]
                )
                fill_in = ws.cell(row=row, column=col).fill
                if isinstance(val_in, datetime):
                    val_in = datetime.date(val_in)
                assert val_in == val_comp
                assert fill_in == (fill if not force else fill_default)


# gen_empty_file_with_style("t1.xlsx")
