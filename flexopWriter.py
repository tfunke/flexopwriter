from pandas.io.excel import (
    ExcelWriter,
    _OpenpyxlWriter,
    register_writer,
    ExcelFile,
)
from pandas.io.excel._util import _validate_freeze_panes
import zipfile
from openpyxl.workbook import Workbook


_flexopWriter_doc = """
This is a slightly modified copy of the pandas ExcelWriter engine
'openpyxl' used by to_excel. It changes the handling of already
existing .xlsx files. It will not delete already existing worksheets.

Adds the option 'force' that, if set to False, only writes the
values to the Excel sheet without applying/changing the cell style.

The name of the engine is 'flex'.

    Usage:
    ------
    Copy flexopWriter.py and make it available for import.

    >>> from pandas import ExcelWriter
    >>> from flexopWriter import _flexopWriter

    Your linter might warn that it´s imported but never used. Just ignore this
    message.

    Then you can use it like this.

    >>> with ExcelWriter('path_to_file.xlsx',
                engine = 'flex',
                mode = {'w'|'a'},
                force = {True|False},
                ) as writer:
    ...     df.to_excel(writer, sheet_name='SheetName')

    If 'path_to_file.xlsx' already exists, all existing sheets with names
    different from 'SheetName' will not be touched and there are the
    following options:

    If 'SheetName' already exists:

        mode = 'w' (default):
            -> 'SheetName' will be overwritten
        mode = 'a':
            -> 'SheetName1' will be appended
            (unless 'SheetName1' already exists. Then another 1 will be
            appended ...)

    If 'SheetName' does not exist, it will be appended independent of mode.

    Force = True (default):
        values and cell style are written
    Force = False
        only values are written

    If 'path_to_file.xlsx' does not exist, it will be created with the sheet
    'SheetName'.

    Derives from class _OpenpyxlWriter
    Overwrites:
        def __init__()
        def write_cells()

    Parameters
    ----------
    path : string
        Path to xlsx file.

    date_format : string, default None
        Format string for dates written into Excel files (e.g. 'YYYY-MM-DD')
    datetime_format : string, default None
        Format string for datetime objects written into Excel files
        (e.g. 'YYYY-MM-DD HH:MM:SS')
    mode : {'w', 'a'}, default 'w'
        File mode to use (write or append).
    force : boolean, default True
        Mode to force rewriting of an existing sheet or just overwrite the
        values

    Methods:
    -------

    def write_cells(
        self, cells, sheet_name=None, startrow=0, startcol=0, freeze_panes=None
    )

    Dependencies:
    -------------
    pandas 0.25
    openpyxl 3.0
    zipfile

Author: Thomas Funke, 11/2019

# Copyright (c) pandas Development Team.
# Distributed under the terms of the Simplified BSD License.
"""


class _flexopWriter(_OpenpyxlWriter):
    engine = "flex"
    supported_extensions = ("xlsx", "xlsm")

    def __init__(
        self, path, engine=None, force=True, mode="w", **engine_kwargs
    ):
        self.force = force
        ExcelWriter.__init__(self, path, mode=mode, **engine_kwargs)
        if not isinstance(path, ExcelFile):
            from openpyxl import load_workbook

            try:  # if the Excelfile can be opened, load the workbook
                book = load_workbook(self.path)
                self.book = book
            except (
                zipfile.BadZipfile,
                FileNotFoundError,
            ):  # else create a new workbook
                self.book = Workbook()
                if self.book.worksheets[0]:
                    if (
                        max(
                            self.book.worksheets[0].max_column,
                            self.book.worksheets[0].max_row,
                        )
                        <= 1
                    ):  # delete the first worksheet if empty
                        try:
                            self.book.remove(self.book.worksheets[0])
                        except AttributeError:
                            # compat - for openpyxl <= 2.4
                            self.book.remove_sheet(self.book.worksheets[0])

    def write_cells(
        self, cells, sheet_name=None, startrow=0, startcol=0, freeze_panes=None
    ):
        # Write the frame cells using openpyxl.
        sheet_name = self._get_sheet_name(sheet_name)

        _style_cache = {}
        if self.mode == "a":
            wks = self.book.create_sheet(title=sheet_name)
        else:
            if sheet_name in self.book.sheetnames:
                if self.force:  # delete existing wks and write a new one
                    try:
                        self.book.remove(self.book[sheet_name])
                    except AttributeError:
                        # compat - for openpyxl <= 2.4
                        self.book.remove_sheet(self.book[sheet_name])
                    wks = self.book.create_sheet(title=sheet_name)
                else:  # keep existing wks
                    wks = self.book[sheet_name]
            else:
                wks = self.book.create_sheet(title=sheet_name)

        if _validate_freeze_panes(freeze_panes):
            wks.freeze_panes = wks.cell(
                row=freeze_panes[0] + 1, column=freeze_panes[1] + 1
            )

        for cell in cells:
            xcell = wks.cell(
                row=startrow + cell.row + 1, column=startcol + cell.col + 1
            )
            if self.force:
                xcell.value, fmt = self._value_with_fmt(cell.val)
                if fmt:
                    xcell.number_format = fmt

                style_kwargs = {}
                if cell.style:
                    key = str(cell.style)
                    style_kwargs = _style_cache.get(key)
                    if style_kwargs is None:
                        style_kwargs = self._convert_to_style_kwargs(
                            cell.style
                        )
                        _style_cache[key] = style_kwargs

                if style_kwargs:
                    for k, v in style_kwargs.items():
                        setattr(xcell, k, v)
            else:  # write into existing wks without changing the style/format
                xcell.value = cell.val

            if cell.mergestart is not None and cell.mergeend is not None:

                wks.merge_cells(
                    start_row=startrow + cell.row + 1,
                    start_column=startcol + cell.col + 1,
                    end_column=startcol + cell.mergeend + 1,
                    end_row=startrow + cell.mergestart + 1,
                )

                # When cells are merged only the top-left cell is preserved
                # The behaviour of the other cells in a merged range is
                # undefined
                if style_kwargs:
                    first_row = startrow + cell.row + 1
                    last_row = startrow + cell.mergestart + 1
                    first_col = startcol + cell.col + 1
                    last_col = startcol + cell.mergeend + 1

                    for row in range(first_row, last_row + 1):
                        for col in range(first_col, last_col + 1):
                            if row == first_row and col == first_col:
                                # Ignore first cell. It is already handled.
                                continue
                            xcell = wks.cell(column=col, row=row)
                            if self.force:
                                for k, v in style_kwargs.items():
                                    setattr(xcell, k, v)


register_writer(_flexopWriter)
